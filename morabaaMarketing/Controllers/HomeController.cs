﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
// ReSharper disable All

namespace morabaaMarketing.Controllers {
    using morabaaMarketing.Models;

    public class HomeController : Controller {
        public ActionResult Index() {
            ViewBag.Title = "Home Page";
            return View();
        }
    }
}
