﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
// ReSharper disable All

namespace morabaaMarketing.Controllers.Api
{
    using System.Web.Http;

    using morabaaMarketing.Models;

    public class CartController : ApiController
    {
        [HttpPost]
        [ActionName("GetShopCarts")]
        public IHttpActionResult getShopCarts(int shopId)
        {
            try
            {
                var result = Cart.getShopCarts(shopId);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("UpdateCartItem")]
        public IHttpActionResult Update(CartItem cartItem) {
            try {
                var result = CartItem.update(cartItem);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
    }
}