﻿using morabaaMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace morabaaMarketing.Controllers.Api {
    public class CommentController : ApiController{


        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(Comment comment) {
            try {
                var result = Comment.add(comment);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public IHttpActionResult delete(Comment comment) {
            try {
                var result = Comment.delete(comment);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

    }
}