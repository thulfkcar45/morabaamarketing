﻿using morabaaMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
// ReSharper disable All

namespace morabaaMarketing.Controllers.Api {
    public class CategoryController : ApiController {

        [HttpPost]
        [ActionName("GetAll")]
        public IHttpActionResult getAll() {
            try {
                var result = Category.get();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(Category category) {
            try { 
                var result = Category.add(category);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }


            /*{
               Category catC1 = new Category() {
                    name = "C1", imageUrl = "hdf", isTermnal = true
                };
                

                Category catC2 = new Category() {
                    name = "C2", imageUrl = "hdf", isTermnal = true
                };
  
                List< Category > catC1C2= new List<Category>() ;
                catC1C2.AddRange(new List<Category> {
                   
                    catC1,catC2
                });
                List<Category> catB = new List<Category>() {new Category(){
                    name = "B", imageUrl = "hj", isTermnal = false
                , categories=catC1C2
                } };
             category = new Category() {
                    name = "A",imageUrl="hj",isTermnal=false
                  ,categories=catB

                };
    "id": 30,
    "name": "A",
    "imageUrl": "hj",
    "deleted": false,
    "items": null,
    "categories": [
        {
            "id": 31,
            "name": "B",
            "imageUrl": "hj",
            "deleted": false,
            "items": null,
            "categories": [
                {
                    "id": 32,
                    "name": "C1",
                    "imageUrl": "hdf",
                    "deleted": false,
                    "items": null,
                    "categories": null,
                    "isTermnal": true
                },
                {
                    "id": 33,
                    "name": "C2",
                    "imageUrl": "hdf",
                    "deleted": false,
                    "items": null,
                    "categories": null,
                    "isTermnal": true
                }
            ],
            "isTermnal": false
        }
    ],
    "isTermnal": false
}*/
        }
        [HttpPost]
        [ActionName("Delete")]
        public IHttpActionResult delete(Category category) {
            try {
                var result = Category.delete(category);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        
        [HttpPost]
        [ActionName("Update")]
        public IHttpActionResult update(Category category) {
            try {
                var result = Category.update(category);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

    }
}