﻿using morabaaMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
// ReSharper disable All

namespace morabaaMarketing.Controllers.Api {
    public class ItemController : ApiController {


        [HttpPost]
        [ActionName("Getlast50Item")]
        public IHttpActionResult getlast50(int lastItemId) {
            try {
                var result = Item.getlast50(lastItemId);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetAll")]
        public IHttpActionResult getall() {
            try {
                var result = Item.getAll();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("GetTopRated")]
        public IHttpActionResult getTopRated(int rowNumber) {
            try {
                var result = Item.getTopRated(rowNumber);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("GetMosetRecent")]
        public IHttpActionResult getMosetRecent() {
            try {
                var result = Item.getMosetRecent();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetMostVisited")]
        public IHttpActionResult getMostVisited() {
            try {
                var result = Item.getMostVisited();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }


        [HttpPost]
        [ActionName("GetMostSaled")]
        public IHttpActionResult getMostSaled() {
            try {
                var result = Item.getMostSaled();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }


        [HttpPost]
        [ActionName("Getlast50ItemByShopId")]
        public IHttpActionResult getlast50ByShopId(int lastItemId, int shopId) {
            try {
                var result = Item.getlast50ByShopId(lastItemId, shopId);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Getlast50ItemByCategoryId")]
        public IHttpActionResult getlast50ByCategoryId(int lastItemId, int categoryId) {
            try {
                var result = Item.getlast50ByCategoryId(lastItemId, categoryId);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("GetItemImges")]
        public IHttpActionResult getItemImges(int itemId) {
            try {
                var result = Item.getItemImages(itemId);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }


        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(Item item) {
            try {
                var result = Item.add(item);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("Update")]
        public IHttpActionResult update(Item item) {
            try {
                var result = Item.update(item);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public IHttpActionResult delete(Item item) {
            try {
                var result = Item.delete(item);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }





    }
}