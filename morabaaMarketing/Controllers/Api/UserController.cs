﻿using morabaaMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace morabaaMarketing.Controllers.Api {
    public class UserController : ApiController {

        [HttpPost]
        [ActionName("AddByPhoneNumber")]
        public IHttpActionResult addByPhoneNumber(User user) {
            try {
                var result = Models.User.addByPhoneNumber(user);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(User user) {
            try {
                var result = Models.User.add(user);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Update")]
        public IHttpActionResult update(User user) {
            try {
                var result = Models.User.update(user);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Delete")]
        public IHttpActionResult delete(User user) {
            try {
                var result = Models.User.delete(user);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetUserById")]
        public IHttpActionResult getUserById(int id) {
            try {
                var result = Models.User.getUserById(id);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetUserByPhoneNumber")]
        public IHttpActionResult getUserByPhoneNumber(string phoneNumber) {
            try {
                var result = Models.User.getUserByPhoneNumber(phoneNumber);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetUserByUserNameAndPassword")]
        public IHttpActionResult getUserByUserNameAndPassword(User user) {
            try {
                var result = Models.User.getUserByUserNameAndPassword(user);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        
       [HttpPost]
        [ActionName("GetAll")]
        public IHttpActionResult getAll() {
            try {
                var result = Models.User.getAll();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

    }
}