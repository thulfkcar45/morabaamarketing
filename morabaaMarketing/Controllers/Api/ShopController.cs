﻿using morabaaMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
// ReSharper disable All

namespace morabaaMarketing.Controllers.Api {
    public class ShopController : ApiController {


        [HttpPost]
        [ActionName("GetMostSaled")]
        public IHttpActionResult getMostSaled() {
            try {
                var result = Shop.getMostSaled();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("GetMostVisited")]
        public IHttpActionResult getMostVisited() {
            try {
                var result = Shop.getMostVisited();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetTopRated")]
        public IHttpActionResult getTopRated() {
            try {
                var result = Shop.getTopRated();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("GetAll")]
        public IHttpActionResult getAll() {
            try {
                var result = Shop.get();
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Delete")]
        public IHttpActionResult delete(Shop shop) {
            try {
                var result = Shop.delete(shop);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Update")]
        public IHttpActionResult update(Shop shop) {
            try {
                var result = Shop.update(shop);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }
        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(Shop shop) {
            try {
                var result = Shop.add(shop);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }

        [HttpPost]
        [ActionName("GetShopByNameAndPassword")]
        public IHttpActionResult getShopByNameAndPassword(Shop shop) {
            try {
                var result = Shop.getShopByNameAndPassword(shop);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }
        }


    }
}