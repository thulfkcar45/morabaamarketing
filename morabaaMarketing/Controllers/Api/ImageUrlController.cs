﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// ReSharper disable All

namespace morabaaMarketing.Controllers.Api {
    using System.Web.Http;

    using morabaaMarketing.Models;

    public class ImageUrlController :ApiController {

        [HttpPost]
        [ActionName("Add")]
        public IHttpActionResult add(List<ImageUrl> imageUrls) {
            try {
                var result = ImageUrl.add(imageUrls);
                if (result == null) return BadRequest("Error");
                return Ok(result);
            } catch (Exception) {
                return InternalServerError();
            }

        }
    }
}