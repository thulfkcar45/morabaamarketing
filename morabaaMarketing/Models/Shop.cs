﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// ReSharper disable All

namespace morabaaMarketing.Models
{
    public class Shop
    {
        #region prop

        public long id { get; set; }

        public string name { get; set; }
        public string imageUrl { get; set; }

        public string gpsLocation { get; set; }

        public string address { get; set; }

        public string type { get; set; }

        public string description { get; set; }

        public bool deleted { get; set; }

        public List<Item> items { get; set; }

        public int visitCount { get; set; }

        public int saleCount { get; set; }

        public List<Category> categories { get; set; }

        public float rating
        {
            get
            {
                return Item.getRatingAverage(this.id);
            }
        }

        public string pssword { get; set; }

        public string phoneNum { get; set; }

        public string governate { get; set; }

        #endregion

        public static List<Shop> get()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.shops.Where(c => c.deleted == false).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Shop> getTopRated()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.shops.OrderByDescending(c => c.rating).Where(c => c.deleted == false).Take(10).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Shop> getMostVisited()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.shops.OrderByDescending(c => c.visitCount).Where(c => c.deleted == false).Take(10)
                        .ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Shop> getMostSaled()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.shops.OrderByDescending(c => c.saleCount).Where(c => c.deleted == false).Take(10)
                        .ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Shop getShopByNameAndPassword(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.shops.FirstOrDefault(
                        c => c.deleted == false && c.name == shop.name && c.pssword == shop.pssword);
                }
            }
            catch (Exception e)
            {
                e.StackTrace.ToString();
                return null;
            }
        }

        public static bool add(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.shops.Add(shop);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool delete(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    shop.deleted = true;
                    ctx.shops.Update(shop);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool update(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.shops.Update(shop);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}