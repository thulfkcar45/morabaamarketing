﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace morabaaMarketing.Models {
    public class Category {
        public long id { get; set; }
        public  string name{ get; set; }
        public string imageUrl  { get; set; }
        public bool deleted { get; set; }
        public List<Item> items { get; set; }
        public List<Category> categories { get; set;}
        public bool isTermnal { get; set; }

        public static List<Category> get() {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.categories.Where(c => c.deleted == false).ToList();
                }
            } catch (Exception) {
                return null;
            }
        }
        public static bool add(Category category) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.categories.Add(category);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool delete(Category category) {
            try {
                using (var ctx = new MarketingContext()) {
                    category.deleted = true;
                    ctx.categories.Update(category);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool update(Category category) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.categories.Update(category);
                    ctx.SaveChanges();
                }

              return true;
            } catch (Exception e) {
                return false;
            }

        }




    }
}