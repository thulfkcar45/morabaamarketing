﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.EntityFrameworkCore;

// ReSharper disable All
namespace morabaaMarketing.Models
{
    public class Item
    {
        #region prop

        public long id { get; set; }

        public string name { get; set; }

        public List<ImageUrl> imageUrls { get; set; }

        public decimal price { get; set; }

        public long shopId { get; set; }

        public long categoryId { get; set; }

        public List<Detail> details { get; set; }

        public int quantity { get; set; }

        public string unit { get; set; }

        public float rating { get; set; }

        /* get {
                     return Comment.getRatingAverage(this.id);
        
                 }
            }  */
        public int rateCount { get; set; }

        public string description { get; set; }

        public DateTime date { get; set; }

        public bool deleted { get; set; }

        public List<Comment> comments { get; set; }

        public int visitCount { get; set; }

        public int saleCount { get; set; }

        #endregion

        public static float getRatingAverage(long categoryId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.Average(c => c.rating);
                }
            }
            catch (Exception)
            {
                return 0.0f;
            }
        }

        internal static Item getItemById(long itemId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.FirstOrDefault(c => c.deleted == false && c.id == itemId);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getAll()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.Where(c => c.deleted == false).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getlast50(int lastItemId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.Where(c => c.id > lastItemId).Take(50).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getlast50ByShopId(int lastItemId, int shopId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.Where(c => c.id > lastItemId && c.shopId == shopId).Take(50).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getlast50ByCategoryId(int lastItemId, int categoryId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.Where(c => c.id > lastItemId && c.categoryId == categoryId).Take(50).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getTopRated(int rowNum)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.FromSql(
                        "WITH MyTable AS" + " (" + "  SELECT *, "
                        + " ROW_NUMBER() OVER(ORDER BY rating DESC) AS 'RowNumber'" + "   FROM Items" + "             )"
                        + "   SELECT top 5 * " + "    FROM MyTable" + "  WHERE RowNumber > " + rowNum).ToList();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Item> getMosetRecent()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.OrderByDescending(c => c.date).Take(10).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getMostVisited()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.OrderByDescending(c => c.visitCount).Take(10).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Item> getMostSaled()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.items.OrderByDescending(c => c.saleCount).Take(10).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Item add(Item item)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.items.Add(item);
                    ctx.SaveChanges();
                    return item;
                }

              
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool delete(Item item)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    item.deleted = true;
                    ctx.items.Update(item);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool update(Item item)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.items.Update(item);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<ImageUrl> getItemImages(int itemId)
        {
            return ImageUrl.getItemImges(itemId);
        }
    }
}