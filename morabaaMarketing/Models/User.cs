﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// ReSharper disable All

namespace morabaaMarketing.Models {
    public class User {
        public long id { get; set; }
        public string name { get; set; }
        public string imageUrl { get; set; }
        public string phoneNumber { get; set; }
        public string Email { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string governate { get; set; }
        public string district { get; set; }
        public string gpsLocation { get; set; }
        public string postalCode { get; set; }
        public bool deleted { get; set; }
        //morabaa user
        public bool IsMorabaaUser { get; set; }
        public Cart cart { get; set; }  
       

        public static bool add(User user) {
            try {

                using (var ctx = new MarketingContext()) {
                    ctx.users.Add(user);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool addByPhoneNumber(User user) {
            try {

                using (var ctx = new MarketingContext()) {


                    user.phoneNumber = trimPhoneNumber(user.phoneNumber);

                    ctx.users.Add(user);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }


        private static string trimPhoneNumber(string phoneNumber) {

            return phoneNumber.Substring(phoneNumber.Length - 10);
        }

        public static bool update(User user) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.users.Update(user);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }

        public static User getUserById(long id) {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.users.FirstOrDefault(c => c.id == id && c.deleted==false);
                }
            } catch (Exception) {
                return null;
            }
        }

        public static bool delete(User user) {
            try {
                using (var ctx = new MarketingContext()) {
                    user.deleted = true;
                    ctx.users.Update(user);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }

        public static User getUserByPhoneNumber(string PhoneNumber) {
            try {
                string number = trimPhoneNumber(PhoneNumber);
                using (var ctx = new MarketingContext()) {
                    return ctx.users.FirstOrDefault(c => c.phoneNumber == number && c.deleted == false);
                }
            } catch (Exception) {
                return null;
            }
        }

        //morabaa user login
        public static User getUserByUserNameAndPassword(User user) {

            try {
                using (var ctx = new MarketingContext()) {
                    if (ctx.users.Where(c => c.IsMorabaaUser == true && c.deleted==false).ToList().Count == 0) {
                        ctx.users.Add(user);
                        ctx.SaveChanges();
                    }
                    return ctx.users.FirstOrDefault(c => c.name == user.name && c.password == user.password && c.IsMorabaaUser == user.IsMorabaaUser);

                }
            } catch (Exception e) {
                return null;
            }
        }


        

        public static List<User> getAll() {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.users.Where(c => c.deleted == false).ToList();
                }
            } catch (Exception) {
                return null;
            }
        }

    }
}