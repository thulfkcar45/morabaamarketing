﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace morabaaMarketing.Models {
    public class Detail {
        public long id { get; set; }
        public string name { get; set; }
        public  string value{ get; set; }
        public long parentId { get; set; }
        public bool deleted { get; set; }


        public static List<Detail> get() {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.detailes.ToList();
                }
            } catch (Exception) {
                return null;
            }
        }

        public static bool Add(Detail detail) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.detailes.Add(detail);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool delete(Detail detail) {
            try {
                using (var ctx = new MarketingContext()) {
                    detail.deleted = true;
                    ctx.detailes.Update(detail);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool update(Detail detail) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.detailes.Update(detail);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }



    }
}