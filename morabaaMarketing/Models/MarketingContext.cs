﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// ReSharper disable All

namespace morabaaMarketing.Models {
    public class MarketingContext : DbContext {

        public DbSet<Item> items { get; set; }
        public DbSet<Comment> comments  { get; set; }
        public DbSet<User> users { get; set; }
      //  public DbSet<Offer> offers  { get; set; }
        public DbSet<ImageUrl> imageUrls { get; set; }
        public DbSet<Category>categories  { get; set; }
        public DbSet<Shop> shops  { get; set; }
        public DbSet<Detail> detailes { get; set; }
        public DbSet<Cart> carts { get; set; }
        public DbSet<CartItem> cartItems { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured) {
              
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-RMGDU7T\\MORABSQLE;Initial Catalog=Market;Integrated Security=true;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<NavigationMenu>();
        }






    }
}