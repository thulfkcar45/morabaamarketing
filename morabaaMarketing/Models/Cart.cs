﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// ReSharper disable All
namespace morabaaMarketing.Models
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class Cart
    {
        public long id { get; set; }

        public long userId { get; set; }
        public List<CartItem> cartItems { get; set; }
        [NotMapped]
        public User user { get; set; }
        
        public static bool add(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.shops.Add(shop);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool update(Shop shop)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.shops.Update(shop);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<Cart> getShopCarts(int shopId)
        {
            List<Cart> carts = new List<Cart>();
            List<CartItem> cartItems = CartItem.getCartItemsByShopId(shopId);
            List<long> usersIdsList = new List<long>();

            foreach (CartItem cartItem in cartItems)
            {
                long cartItemUserId = getCartItemUserIdByCartId(cartItem.cartId);
                if (!usersIdsList.Contains(cartItemUserId))
                {
                    usersIdsList.Add(cartItemUserId);
                }
            }

            foreach (var userId in usersIdsList)
            {
                List<CartItem> cis = new List<CartItem>();
                foreach (CartItem cartItem in cartItems)
                {
                    if (getCartItemUserIdByCartId(cartItem.cartId) == userId)
                    {
                        cis.Add(cartItem);
                    }
                }

                carts.Add(new Cart { user = User.getUserById(userId), cartItems = cis });
            }

            return carts;
        }

        private static long getCartItemUserIdByCartId(long cartId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.carts.FirstOrDefault(c => c.id == cartId).id;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static Cart getCartByUserId(long userId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.carts.FirstOrDefault(c => c.userId == userId);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Cart> getAll()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.carts.ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
     
    }
}