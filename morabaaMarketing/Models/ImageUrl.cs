﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable All
namespace morabaaMarketing.Models {
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net.Mime;

    public class ImageUrl {
        public long id { get; set; }

        public string url { get; set; }

        public bool main { get; set; }

        public long itemId { get; set; }

        public bool deleted { get; set; }
        public byte[] imageByte { get; set; }

        [NotMapped]
        public byte[] image { get; set; }

        public static List<ImageUrl> get() {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.imageUrls.ToList();
                }
            } catch (Exception) {
                return null;
            }
        }

        public static bool addToDB(ImageUrl imageUrl) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.imageUrls.Add(imageUrl);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }
        }

        public static bool update(ImageUrl imageUrl) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.imageUrls.Update(imageUrl);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }
        }

        public static bool delete(ImageUrl imageUrl) {
            try {
                using (var ctx = new MarketingContext()) {
                    imageUrl.deleted = true;
                    ctx.imageUrls.Update(imageUrl);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }
        }

        public static List<ImageUrl> getItemImges(int itemId) {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.imageUrls.Where(c => c.itemId == itemId).ToList();
                }
            } catch (Exception) {
                return null;
            }
        }

        public static List<string> addToDirectory(List<ImageUrl> imageUrls) {
            try {
                string itemImagesParth;
                string path = AppDomain.CurrentDomain.BaseDirectory;
                if (!Directory.Exists(path + "images")) {
                    Directory.CreateDirectory(path + "images");
                    add(imageUrls);
                } else {
                    int i = 0;
                    foreach (ImageUrl imageUrl in imageUrls) {
                        i++;
                        saveToDirectory(imageUrl, i);
                    }
                }

                return null;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void saveToDirectory(ImageUrl imageUrl, int i) {
            string itemImagesParth;

            string path = AppDomain.CurrentDomain.BaseDirectory;

            byte[] bitmap = imageUrl.image;

            itemImagesParth = path + "images\\" + imageUrl.itemId.ToString();

            if (!Directory.Exists(itemImagesParth)) {
                Directory.CreateDirectory(itemImagesParth);
                saveToDirectory(imageUrl, i);
            } else {
                string Savepath = itemImagesParth ;

                try {
                    //using (Image image = Image.FromStream(new MemoryStream(bitmap))) {
                    //    image.Save(Savepath, ImageFormat.Png); // Or Png



                    //}

                    using (MemoryStream memory = new MemoryStream(bitmap)) {
                        Image image = Image.FromStream(memory);
                        using (FileStream fs = new FileStream(Savepath,FileMode.Open,FileAccess.ReadWrite)) {
                            image.Save(memory, ImageFormat.Jpeg);
                            //byte[] bytes = memory.ToArray();
                            //fs.Write(bytes, 0, bytes.Length);
                        }
                    }


                    imageUrl.url = "images\\" + imageUrl.itemId.ToString() + "\\" + i.ToString() + ".png";
                } catch (Exception e) { e.StackTrace.ToString(); }
                addToDB(imageUrl);
            }
        }
        public static bool add(List<ImageUrl> imageUrls) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.imageUrls.AddRange(imageUrls);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }
        }
    }
}