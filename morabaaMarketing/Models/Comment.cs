﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace morabaaMarketing.Models {
    public class Comment {
        public long id { get; set; }
        public float rating { get; set; }
        public string text  { get; set; }
        public  string date{ get; set; }
        public  long itemId{ get; set; }
        public  User user{ get; set; }
        public bool deleted { get; set; }

        public static List<Comment> get() {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.comments.ToList();
                }
            } catch (Exception) {
                return null;
            }
        }

        public static float getRatingAverage(long itemId) {
            try {
                using (var ctx = new MarketingContext()) {
                    return ctx.comments.Average(c=>c.rating);
                }
            } catch (Exception) {
                return 0.0f;
            }

        }
        public static bool add(Comment comment) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.comments.Add(comment);                 
                    ctx.SaveChanges();

                    Item item = Item.getItemById(comment.itemId);
                    item.rating = Comment.getRatingAverage(comment.itemId);
                    Item.update(item);
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool delete(Comment comment) {
            try {
                using (var ctx = new MarketingContext()) {
                    comment.deleted = true;
                    ctx.comments.Update(comment);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }
        public static bool update(Comment comment) {
            try {
                using (var ctx = new MarketingContext()) {
                    ctx.comments.Update(comment);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception) {
                return false;
            }

        }



    }
}