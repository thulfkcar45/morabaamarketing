﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace morabaaMarketing.Models {
    public class Offer {
        public long id { get; set; }
        public List<Item> items { get; set; }
        public string discription  {  get; set; }
        public  string imageUrl {  get; set; }
        public string name  {  get; set; }
        public  DateTime startDate {  get; set; }
        public int duration  {  get; set; }
        public bool deleted { get; set; }

    }
}