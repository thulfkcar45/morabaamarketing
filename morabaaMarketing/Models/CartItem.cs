﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace morabaaMarketing.Models
{
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.EntityFrameworkCore;

    [SuppressMessage("ReSharper", "StyleCop.SA1300")]
    public class CartItem
    {
        public long id { get; set; }

        public long cartId { get; set; }

        public Item item { get; set; }

        public int count { get; set; }

        public bool delivered { get; set; }

        public bool deleted { get; set; }

        public static List<CartItem> getAll()
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.cartItems.Where(c => c.deleted == false).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<CartItem> getDelivered(bool isDelivered)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.cartItems.Where(c => c.deleted == false && c.delivered == isDelivered).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<CartItem> getCartItemByCartId(int id)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    return ctx.cartItems.Where(c => c.deleted == false && c.id == id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static List<CartItem> getCartItemsByShopId(long shopId)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    List<CartItem> cartItems= ctx.cartItems.Include(c=> c.item).Where(c => c.item.shopId == shopId && c.delivered==false && c.deleted==false).ToList();
                
                    return cartItems;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static bool add(CartItem cartItem)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.cartItems.Add(cartItem);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool delete(CartItem cartItem)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    cartItem.deleted = true;
                    ctx.cartItems.Update(cartItem);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool update(CartItem cartItem)
        {
            try
            {
                using (var ctx = new MarketingContext())
                {
                    ctx.cartItems.Update(cartItem);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}