﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "rateCont",
                table: "items",
                newName: "rateCount");

            migrationBuilder.AddColumn<float>(
                name: "rating",
                table: "items",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rating",
                table: "items");

            migrationBuilder.RenameColumn(
                name: "rateCount",
                table: "items",
                newName: "rateCont");
        }
    }
}
