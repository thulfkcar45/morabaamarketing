﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Shopid",
                table: "categories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_categories_Shopid",
                table: "categories",
                column: "Shopid");

            migrationBuilder.AddForeignKey(
                name: "FK_categories_shops_Shopid",
                table: "categories",
                column: "Shopid",
                principalTable: "shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_categories_shops_Shopid",
                table: "categories");

            migrationBuilder.DropIndex(
                name: "IX_categories_Shopid",
                table: "categories");

            migrationBuilder.DropColumn(
                name: "Shopid",
                table: "categories");
        }
    }
}
