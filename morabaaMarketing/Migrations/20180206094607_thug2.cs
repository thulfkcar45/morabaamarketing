﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_items_shopId",
                table: "items",
                column: "shopId");

            migrationBuilder.AddForeignKey(
                name: "FK_items_shops_shopId",
                table: "items",
                column: "shopId",
                principalTable: "shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_items_shops_shopId",
                table: "items");

            migrationBuilder.DropIndex(
                name: "IX_items_shopId",
                table: "items");
        }
    }
}
