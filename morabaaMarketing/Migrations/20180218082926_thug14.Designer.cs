﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using morabaaMarketing.Models;

namespace morabaaMarketing.Migrations
{
    [DbContext(typeof(MarketingContext))]
    [Migration("20180218082926_thug14")]
    partial class thug14
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("morabaaMarketing.Models.Cart", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("userId");

                    b.HasKey("id");

                    b.HasIndex("userId")
                        .IsUnique();

                    b.ToTable("carts");
                });

            modelBuilder.Entity("morabaaMarketing.Models.CartItem", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("cartId");

                    b.Property<int>("count");

                    b.Property<bool>("deleted");

                    b.Property<bool>("delivered");

                    b.Property<long?>("itemid");

                    b.HasKey("id");

                    b.HasIndex("cartId");

                    b.HasIndex("itemid");

                    b.ToTable("cartItems");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Category", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("Categoryid");

                    b.Property<long?>("Shopid");

                    b.Property<bool>("deleted");

                    b.Property<string>("imageUrl");

                    b.Property<bool>("isTermnal");

                    b.Property<string>("name");

                    b.HasKey("id");

                    b.HasIndex("Categoryid");

                    b.HasIndex("Shopid");

                    b.ToTable("categories");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Comment", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("date");

                    b.Property<bool>("deleted");

                    b.Property<long>("itemId");

                    b.Property<float>("rating");

                    b.Property<string>("text");

                    b.Property<long?>("userid");

                    b.HasKey("id");

                    b.HasIndex("itemId");

                    b.HasIndex("userid");

                    b.ToTable("comments");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Detail", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("Itemid");

                    b.Property<bool>("deleted");

                    b.Property<string>("name");

                    b.Property<long>("parentId");

                    b.Property<string>("value");

                    b.HasKey("id");

                    b.HasIndex("Itemid");

                    b.ToTable("detailes");
                });

            modelBuilder.Entity("morabaaMarketing.Models.ImageUrl", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("deleted");

                    b.Property<long>("itemId");

                    b.Property<bool>("main");

                    b.Property<string>("url");

                    b.HasKey("id");

                    b.HasIndex("itemId");

                    b.ToTable("imageUrls");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Item", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("categoryId");

                    b.Property<DateTime>("date");

                    b.Property<bool>("deleted");

                    b.Property<string>("description");

                    b.Property<string>("name");

                    b.Property<decimal>("price");

                    b.Property<int>("quantity");

                    b.Property<int>("rateCount");

                    b.Property<float>("rating");

                    b.Property<int>("saleCount");

                    b.Property<long>("shopId");

                    b.Property<string>("unit");

                    b.Property<int>("visitCount");

                    b.HasKey("id");

                    b.HasIndex("categoryId");

                    b.HasIndex("shopId");

                    b.ToTable("items");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Shop", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("address");

                    b.Property<bool>("deleted");

                    b.Property<string>("description");

                    b.Property<string>("governate");

                    b.Property<string>("gpsLocation");

                    b.Property<string>("imageUrl");

                    b.Property<string>("name");

                    b.Property<string>("phoneNum");

                    b.Property<string>("pssword");

                    b.Property<int>("saleCount");

                    b.Property<string>("type");

                    b.Property<int>("visitCount");

                    b.HasKey("id");

                    b.ToTable("shops");
                });

            modelBuilder.Entity("morabaaMarketing.Models.User", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<bool>("IsMorabaaUser");

                    b.Property<string>("address");

                    b.Property<bool>("deleted");

                    b.Property<string>("district");

                    b.Property<string>("governate");

                    b.Property<string>("gpsLocation");

                    b.Property<string>("imageUrl");

                    b.Property<string>("name");

                    b.Property<string>("password");

                    b.Property<string>("phoneNumber");

                    b.Property<string>("postalCode");

                    b.HasKey("id");

                    b.ToTable("users");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Cart", b =>
                {
                    b.HasOne("morabaaMarketing.Models.User")
                        .WithOne("cart")
                        .HasForeignKey("morabaaMarketing.Models.Cart", "userId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("morabaaMarketing.Models.CartItem", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Cart")
                        .WithMany("cartItems")
                        .HasForeignKey("cartId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("morabaaMarketing.Models.Item", "item")
                        .WithMany()
                        .HasForeignKey("itemid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Category", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Category")
                        .WithMany("categories")
                        .HasForeignKey("Categoryid");

                    b.HasOne("morabaaMarketing.Models.Shop")
                        .WithMany("categories")
                        .HasForeignKey("Shopid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Comment", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("comments")
                        .HasForeignKey("itemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("morabaaMarketing.Models.User", "user")
                        .WithMany()
                        .HasForeignKey("userid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Detail", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("details")
                        .HasForeignKey("Itemid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.ImageUrl", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("imageUrls")
                        .HasForeignKey("itemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("morabaaMarketing.Models.Item", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Category")
                        .WithMany("items")
                        .HasForeignKey("categoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("morabaaMarketing.Models.Shop")
                        .WithMany("items")
                        .HasForeignKey("shopId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
