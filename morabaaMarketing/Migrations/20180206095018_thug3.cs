﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Itemid",
                table: "categories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_comments_itemId",
                table: "comments",
                column: "itemId");

            migrationBuilder.CreateIndex(
                name: "IX_categories_Itemid",
                table: "categories",
                column: "Itemid");

            migrationBuilder.AddForeignKey(
                name: "FK_categories_items_Itemid",
                table: "categories",
                column: "Itemid",
                principalTable: "items",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_comments_items_itemId",
                table: "comments",
                column: "itemId",
                principalTable: "items",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_categories_items_Itemid",
                table: "categories");

            migrationBuilder.DropForeignKey(
                name: "FK_comments_items_itemId",
                table: "comments");

            migrationBuilder.DropIndex(
                name: "IX_comments_itemId",
                table: "comments");

            migrationBuilder.DropIndex(
                name: "IX_categories_Itemid",
                table: "categories");

            migrationBuilder.DropColumn(
                name: "Itemid",
                table: "categories");
        }
    }
}
