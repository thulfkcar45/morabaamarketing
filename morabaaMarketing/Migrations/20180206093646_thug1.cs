﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_comments_users_userId",
                table: "comments");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "users",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "discription",
                table: "shops",
                newName: "description");

            migrationBuilder.RenameColumn(
                name: "userId",
                table: "comments",
                newName: "userid");

            migrationBuilder.RenameIndex(
                name: "IX_comments_userId",
                table: "comments",
                newName: "IX_comments_userid");

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "shops",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "imageUrls",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "detailes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "categories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_comments_users_userid",
                table: "comments",
                column: "userid",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_comments_users_userid",
                table: "comments");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "users");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "shops");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "items");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "imageUrls");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "detailes");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "comments");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "categories");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "users",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "shops",
                newName: "discription");

            migrationBuilder.RenameColumn(
                name: "userid",
                table: "comments",
                newName: "userId");

            migrationBuilder.RenameIndex(
                name: "IX_comments_userid",
                table: "comments",
                newName: "IX_comments_userId");

            migrationBuilder.AddForeignKey(
                name: "FK_comments_users_userId",
                table: "comments",
                column: "userId",
                principalTable: "users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
