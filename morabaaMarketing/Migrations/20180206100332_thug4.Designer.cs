﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using morabaaMarketing.Models;

namespace morabaaMarketing.Migrations
{
    [DbContext(typeof(MarketingContext))]
    [Migration("20180206100332_thug4")]
    partial class thug4
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("morabaaMarketing.Models.Category", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("deleted");

                    b.Property<string>("imageUrl");

                    b.Property<string>("name");

                    b.HasKey("id");

                    b.ToTable("categories");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Comment", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("date");

                    b.Property<bool>("deleted");

                    b.Property<long>("itemId");

                    b.Property<float>("rating");

                    b.Property<string>("text");

                    b.Property<long?>("userid");

                    b.HasKey("id");

                    b.HasIndex("itemId");

                    b.HasIndex("userid");

                    b.ToTable("comments");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Detail", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("Itemid");

                    b.Property<bool>("deleted");

                    b.Property<string>("name");

                    b.Property<long>("parentId");

                    b.Property<string>("value");

                    b.HasKey("id");

                    b.HasIndex("Itemid");

                    b.ToTable("detailes");
                });

            modelBuilder.Entity("morabaaMarketing.Models.ImageUrl", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("deleted");

                    b.Property<long>("itemId");

                    b.Property<bool>("main");

                    b.Property<string>("url");

                    b.HasKey("id");

                    b.HasIndex("itemId");

                    b.ToTable("imageUrls");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Item", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("categoryId");

                    b.Property<DateTime>("date");

                    b.Property<bool>("deleted");

                    b.Property<string>("description");

                    b.Property<string>("name");

                    b.Property<decimal>("price");

                    b.Property<int>("quantity");

                    b.Property<int>("rateCont");

                    b.Property<float>("rating");

                    b.Property<long>("shopId");

                    b.Property<string>("unit");

                    b.HasKey("id");

                    b.HasIndex("categoryId");

                    b.HasIndex("shopId");

                    b.ToTable("items");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Shop", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("address");

                    b.Property<bool>("deleted");

                    b.Property<string>("description");

                    b.Property<string>("gpsLocation");

                    b.Property<string>("name");

                    b.Property<string>("type");

                    b.HasKey("id");

                    b.ToTable("shops");
                });

            modelBuilder.Entity("morabaaMarketing.Models.User", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("address");

                    b.Property<bool>("deleted");

                    b.Property<string>("district");

                    b.Property<string>("governate");

                    b.Property<string>("gpsLocation");

                    b.Property<string>("imageUrl");

                    b.Property<string>("name");

                    b.Property<string>("password");

                    b.Property<string>("phoneNumber");

                    b.Property<string>("postalCode");

                    b.HasKey("id");

                    b.ToTable("users");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Comment", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("comments")
                        .HasForeignKey("itemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("morabaaMarketing.Models.User", "user")
                        .WithMany()
                        .HasForeignKey("userid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.Detail", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("details")
                        .HasForeignKey("Itemid");
                });

            modelBuilder.Entity("morabaaMarketing.Models.ImageUrl", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Item")
                        .WithMany("imageUrls")
                        .HasForeignKey("itemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("morabaaMarketing.Models.Item", b =>
                {
                    b.HasOne("morabaaMarketing.Models.Category")
                        .WithMany("items")
                        .HasForeignKey("categoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("morabaaMarketing.Models.Shop")
                        .WithMany("items")
                        .HasForeignKey("shopId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
