﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "phoneNum",
                table: "shops",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pssword",
                table: "shops",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "phoneNum",
                table: "shops");

            migrationBuilder.DropColumn(
                name: "pssword",
                table: "shops");
        }
    }
}
