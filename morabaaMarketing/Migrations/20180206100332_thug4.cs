﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_categories_items_Itemid",
                table: "categories");

            migrationBuilder.DropIndex(
                name: "IX_categories_Itemid",
                table: "categories");

            migrationBuilder.DropColumn(
                name: "Itemid",
                table: "categories");

            migrationBuilder.RenameColumn(
                name: "discription",
                table: "items",
                newName: "description");

            migrationBuilder.CreateIndex(
                name: "IX_items_categoryId",
                table: "items",
                column: "categoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_items_categories_categoryId",
                table: "items",
                column: "categoryId",
                principalTable: "categories",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_items_categories_categoryId",
                table: "items");

            migrationBuilder.DropIndex(
                name: "IX_items_categoryId",
                table: "items");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "items",
                newName: "discription");

            migrationBuilder.AddColumn<long>(
                name: "Itemid",
                table: "categories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_categories_Itemid",
                table: "categories",
                column: "Itemid");

            migrationBuilder.AddForeignKey(
                name: "FK_categories_items_Itemid",
                table: "categories",
                column: "Itemid",
                principalTable: "items",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
