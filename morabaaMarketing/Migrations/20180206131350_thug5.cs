﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace morabaaMarketing.Migrations
{
    public partial class thug5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rating",
                table: "items");

            migrationBuilder.AddColumn<int>(
                name: "saleCount",
                table: "shops",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "visitCount",
                table: "shops",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "saleCount",
                table: "items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "visitCount",
                table: "items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "Categoryid",
                table: "categories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isTermnal",
                table: "categories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_categories_Categoryid",
                table: "categories",
                column: "Categoryid");

            migrationBuilder.AddForeignKey(
                name: "FK_categories_categories_Categoryid",
                table: "categories",
                column: "Categoryid",
                principalTable: "categories",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_categories_categories_Categoryid",
                table: "categories");

            migrationBuilder.DropIndex(
                name: "IX_categories_Categoryid",
                table: "categories");

            migrationBuilder.DropColumn(
                name: "saleCount",
                table: "shops");

            migrationBuilder.DropColumn(
                name: "visitCount",
                table: "shops");

            migrationBuilder.DropColumn(
                name: "saleCount",
                table: "items");

            migrationBuilder.DropColumn(
                name: "visitCount",
                table: "items");

            migrationBuilder.DropColumn(
                name: "Categoryid",
                table: "categories");

            migrationBuilder.DropColumn(
                name: "isTermnal",
                table: "categories");

            migrationBuilder.AddColumn<float>(
                name: "rating",
                table: "items",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
